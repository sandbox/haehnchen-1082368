<?php

/**
 * @file disqus_backup_comment.tpl
 * Default theme implementation for disqus backup.
 *
 * Available variables:
 * - $author_name: Comment author. Can be link or plain text.
 * - $message: Body of the post.
 * - $created_at: Date and time of posting.
 * - $author_mail
 * - $status
 * - $author_url
 *
 * @see template_preprocess_disqus_backup_comment()
 * @see theme_disqus_backup()
 *
 */ 
?>
 <a id="comment-<?php print $did ?>-did"></a>
 <div class="comment<?php print ' '. $status ?> clear-block">

 <div class="comment-inner clear-block">
    <div class="submitted">
      <?php print $submitted ?>
    </div>

  
  <div class="content">
    <?php print $content ?>
  </div>
 </div>

</div>